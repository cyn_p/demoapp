package mx.aliada.login.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.HashMap;
import java.util.Map;

import mx.aliada.R;
import mx.aliada.login.model.AliadaProfile;
import mx.aliada.utils.Utils;
import mx.aliada.general.MainActivity;

/**
 * Created by Cynthia Palma on 23/06/16.
 */

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    private EditText editAliadaID;
    private EditText editAliadaMail;
    private EditText editPassword;
    private ProgressBar progressLogin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView() {
        editAliadaID = (EditText) findViewById(R.id.edit_aliada_id);
        editAliadaMail = (EditText) findViewById(R.id.edit_aliada_mail);
        editPassword = (EditText) findViewById(R.id.edit_password);
        progressLogin = (ProgressBar) findViewById(R.id.progress_login);

        Button btnLogin = (Button) findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifySession(editAliadaID.getText().toString(), editAliadaMail.getText().toString(),
                        editPassword.getText().toString());
            }
        });
    }

    private void verifySession(final String aliadaID, final String aliadaMail, final String password) {
        if (validateEmptyStrings(aliadaID, aliadaMail, password)) {
            progressLogin.setVisibility(View.VISIBLE);
            RequestQueue queue = Volley.newRequestQueue(this);

            StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST,
                    "http://api.staging.aliada.mx/sessions", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG, "onResponse() called with: response = [" + response + "]");
                    JsonElement jelement = new JsonParser().parse(response);
                    final JsonObject jsonObject = jelement.getAsJsonObject();
                    Gson gson = new Gson();
                    final AliadaProfile aliadaProfile = gson.fromJson(jsonObject.get("session"), AliadaProfile.class);
                    Log.d(TAG, "onResponse: " + aliadaProfile.toString());
                    aliadaProfile.generateName();

                    Utils.putAliadaProfileToSP(LoginActivity.this, aliadaProfile);

                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    finish();
                    startActivity(i);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "onErrorResponse() called with: error = [" + error + "]");
                    progressLogin.setVisibility(View.INVISIBLE);
                    editAliadaID.setError(getString(R.string.str_error_wrong_field));
                    editAliadaMail.setError(getString(R.string.str_error_wrong_field));
                    editPassword.setError(getString(R.string.str_error_wrong_field));
                    Snackbar.make(editAliadaID, R.string.str_error_snack_wrong_field, Snackbar.LENGTH_SHORT).show();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Accept", "application/vnd.aliada.v1");
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("email", aliadaMail);
                    params.put("password", password);
                    params.put("aliada_id", aliadaID);
                    return params;
                }
            };
            queue.add(jsonObjectRequest);
        }
    }

    private boolean validateEmptyStrings(String aliadaID, String aliadaMail, String password) {
        boolean valid = true;
        if (aliadaID.isEmpty()) {
            valid = false;
            editAliadaID.setError(getString(R.string.str_error_empty_field));
        }
        if (aliadaMail.isEmpty()) {
            valid = false;
            editAliadaMail.setError(getString(R.string.str_error_empty_field));
        }
        if (password.isEmpty()) {
            valid = false;
            editPassword.setError(getString(R.string.str_error_empty_field));
        }
        return valid;
    }
}
