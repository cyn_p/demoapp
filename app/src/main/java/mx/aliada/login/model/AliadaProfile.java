package mx.aliada.login.model;

import android.util.Log;

import mx.aliada.general.GeneralProfile;

/**
 * Created by Cynthia Palma on 23/06/16.
 */

public class AliadaProfile extends GeneralProfile {

    private static final String TAG = "AliadaProfile";
    private String api_token;
    private String name;

    public String getApiToken() {
        return api_token;
    }

    public void setApiToken(String apiToken) {
        this.api_token = apiToken;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void generateName() {
        if (getFullName().isEmpty()) {
            this.name = "";
        } else {
            Log.d(TAG, "getName() called" + getFullName());
            final String[] split = getFullName().split(" ");
            Log.d(TAG, "getName() called" + split);
            this.name = split[0];
        }
    }

    @Override
    public String toString() {
        return getId() + getFullName() + getEmail() + getApiToken();
    }
}
