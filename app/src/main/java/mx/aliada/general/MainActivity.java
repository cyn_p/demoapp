package mx.aliada.general;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import mx.aliada.R;
import mx.aliada.utils.Utils;
import mx.aliada.login.model.AliadaProfile;
import mx.aliada.services.view.ServicesFragment;

/**
 * Created by Cynthia Palma on 23/06/16.
 */

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private int navDrawerCurrentItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final AliadaProfile aliadaProfile = Utils.getAliadaProfileFromSP(this);
        initView(aliadaProfile);
    }

    private void initView(AliadaProfile aliadaProfile) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //Get the navigation drawer and sets the listener
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Set the current selection to ShakeFragment because is the default
        navDrawerCurrentItem = R.id.nav_services;

        //First we need to get the header of the nav drawer in order to get its elements
        final View headerView = navigationView.getHeaderView(0);
        TextView txtUsername = (TextView) headerView.findViewById(R.id.txt_nav_user);
        txtUsername.setText(String.format(getString(R.string.str_welcome), aliadaProfile.getName()));
        TextView txtEmail = (TextView) headerView.findViewById(R.id.txt_nav_mail);
        txtEmail.setText(aliadaProfile.getEmail());

        //Create and show an instance of fragment services
        Fragment fragment = ServicesFragment.newInstance();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_services) {
            // Show Fragment services
        } else if (id == R.id.nav_settings) {
            //Show Settings fragment
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
