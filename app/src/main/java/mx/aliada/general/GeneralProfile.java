package mx.aliada.general;

/**
 * Created by Cynthia Palma on 23/06/16.
 */

public class GeneralProfile {

    private int id;
    private String email;
    private String full_name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return full_name;
    }

    public void setFullName(String fullName) {
        this.full_name = fullName;
    }

    @Override
    public String toString() {
        return "GeneralProfile{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", full_name='" + full_name + '\'' +
                '}';
    }
}
