package mx.aliada.utils;

import java.util.Locale;

/**
 * Created by cyn  son 06/24/2016.
 */

public class AliadaConstants {
    //Shared preferences constants
    static final String SP_USER_NAME = "spUserName";
    static final String SP_EMAIL = "spEmail";
    static final String SP_API_TOKEN = "spApiToken";
    //Util constants
    static final String EMPTY_STRING = "spApiToken";
    //Datetime constants
    public static final Locale LOCALE_MX = new Locale("es", "MX");
}
