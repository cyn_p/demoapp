package mx.aliada.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.text.SimpleDateFormat;
import java.util.Date;

import mx.aliada.login.model.AliadaProfile;

import static mx.aliada.utils.AliadaConstants.EMPTY_STRING;
import static mx.aliada.utils.AliadaConstants.LOCALE_MX;
import static mx.aliada.utils.AliadaConstants.SP_API_TOKEN;
import static mx.aliada.utils.AliadaConstants.SP_EMAIL;
import static mx.aliada.utils.AliadaConstants.SP_USER_NAME;

/**
 * Created by cyn  son 06/24/2016.
 */

public class Utils {

    /**
     * Method that puts on Shared Preferences the aliada information
     *
     * @param context
     * @param aliadaProfile
     */
    public static void putAliadaProfileToSP(Context context, AliadaProfile aliadaProfile) {
        //Get an instance of SharedPreferences
        final SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(SP_USER_NAME, aliadaProfile.getName());
        editor.putString(SP_EMAIL, aliadaProfile.getEmail());
        editor.putString(SP_API_TOKEN, aliadaProfile.getApiToken());
        editor.apply();
    }

    /**
     * Method that retrieves from Shared Preferences the aliada information
     *
     * @param context
     */
    public static AliadaProfile getAliadaProfileFromSP(Context context) {
        //Get an instance of SharedPreferences
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        AliadaProfile aliadaProfile = new AliadaProfile();
        aliadaProfile.setName(sharedPreferences.getString(SP_USER_NAME, EMPTY_STRING));
        aliadaProfile.setEmail(sharedPreferences.getString(SP_EMAIL, EMPTY_STRING));
        aliadaProfile.setApiToken(sharedPreferences.getString(SP_API_TOKEN, EMPTY_STRING));
        return aliadaProfile;
    }

    /**
     * Method that retrieves from Shared Preferences the API Token in order to make http requests
     *
     * @param context
     */
    public static String getApiTokenFromSP(Context context) {
        //Get an instance of SharedPreferences
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(SP_API_TOKEN, EMPTY_STRING);
    }

    /**
     * Method that format an object Date into a useful string for the UI
     *
     * @param date
     */
    public static String parseDateToString(Date date) {
        //21-oct-2014 / 5:44 AM
        String sDate = "-";
        if (date != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", LOCALE_MX);
            sDate = simpleDateFormat.format(date);
        }
        return sDate;
    }
}
