package mx.aliada.services.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import mx.aliada.R;
import mx.aliada.services.model.Service;
import mx.aliada.utils.Utils;

/**
 * Created by Cynthia Palma on 23/06/16.
 */

public class ServicesFragment extends Fragment {

    private static final String TAG = "ServicesFragment";
    private List<Service> services;
    private ImageView imgCollapsing;
    private ServicesAdapter rowsRecyclerAdapter;
    private RequestQueue queue;

    public static ServicesFragment newInstance() {
        ServicesFragment fragment = new ServicesFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        services = new LinkedList<>();
        getServicesFromServer();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //The app bar is expanded for default
        AppBarLayout appBarLayout = (AppBarLayout) getActivity().findViewById(R.id.app_bar_layout);
        appBarLayout.setExpanded(true);

        //Put the title and image view according to the fragment
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) appBarLayout.findViewById(R.id.collapsing_toolbar_layout);
        collapsingToolbarLayout.setTitle(getString(R.string.str_fragment_services_title));
        imgCollapsing = (ImageView) appBarLayout.findViewById(R.id.collapsing_image);
        imgCollapsing.setImageResource(R.drawable.home_cleaning);

        View fragmentView = inflater.inflate(R.layout.fragment_services, container, false);

        //Set the adapter to the recycler view
        RecyclerView firstRecyclerView = (RecyclerView) fragmentView.findViewById(R.id.recycler_services);
        firstRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        firstRecyclerView.setHasFixedSize(true);
        rowsRecyclerAdapter = new ServicesAdapter(services, getContext());
        firstRecyclerView.setAdapter(rowsRecyclerAdapter);

        return fragmentView;
    }


    private void getServicesFromServer() {
        queue = Volley.newRequestQueue(getContext());
        //We use a string request in order to override the headers and params of the request
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET,
                "http://api.staging.aliada.mx/services", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse() called with: response = [" + response + "]");
                //Convert the string response to Json Object and then
                //generate Service objects
                JsonElement jelement = new JsonParser().parse(response);
                final JsonObject jsonObject = jelement.getAsJsonObject();
                final Gson gson = new GsonBuilder().create();
                services = gson.fromJson(jsonObject.getAsJsonArray("services"),
                        new TypeToken<List<Service>>() {
                        }.getType());
                for (Service service : services) {
                    Log.i("Service Details", service.toString());
                }
                //Once we get the services, notify to the adapter
                rowsRecyclerAdapter.updateItems(services);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse() called with: error = [" + error + "]");
                Snackbar.make(imgCollapsing, R.string.str_error_snack_services, Snackbar.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/vnd.aliada.v1");
                headers.put("Authorization", Utils.getApiTokenFromSP(getContext()));
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("limit", "20");
                params.put("page", "1");
                return params;
            }
        };
        queue.add(jsonObjectRequest);
    }

    @Override
    public void onStop() {
        super.onStop();
        //Stop the volley queue
        queue.stop();
    }
}
