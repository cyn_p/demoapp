package mx.aliada.services.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import mx.aliada.R;
import mx.aliada.services.model.Service;
import mx.aliada.utils.Utils;

/**
 * Created by Cynthia Palma on 23/06/16.
 */

class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {
    private List<Service> items;
    private Context context;

    ServicesAdapter(List<Service> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        //(LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.service_element, parent, false);
        return new ViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Service service = items.get(position);
        holder.txtServiceType.setText(String.format(context.getString(R.string.str_service_type), service.getType()));
        holder.txtUserFullName.setText(service.getUser().getFullName());
        holder.txtUserPhone.setText(service.getUser().getPhone());
        holder.txtServiceDatetime.setText(Utils.parseDateToString(service.getDatetime()));
        holder.txtServiceStreet.setText(service.getAddress().getStreet());
        holder.txtServiceNeighborhood.setText(service.getAddress().getColony());
        holder.txtServiceCity.setText(service.getAddress().getCity());
    }

    void updateItems(List<Service> services) {
        this.items = services;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtServiceType;
        TextView txtUserFullName;
        TextView txtUserPhone;
        TextView txtServiceDatetime;
        TextView txtServiceStreet;
        TextView txtServiceNeighborhood;
        TextView txtServiceCity;


        ViewHolder(View itemView) {
            super(itemView);
            txtServiceType = (TextView) itemView.findViewById(R.id.txt_service_type);
            txtUserFullName = (TextView) itemView.findViewById(R.id.txt_user_full_name);
            txtUserPhone = (TextView) itemView.findViewById(R.id.txt_user_phone);
            txtServiceDatetime = (TextView) itemView.findViewById(R.id.txt_service_datetime);
            txtServiceStreet = (TextView) itemView.findViewById(R.id.txt_service_street);
            txtServiceNeighborhood = (TextView) itemView.findViewById(R.id.txt_service_neighborhood);
            txtServiceCity = (TextView) itemView.findViewById(R.id.txt_service_city);
        }
    }
}
