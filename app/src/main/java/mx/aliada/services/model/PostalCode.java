package mx.aliada.services.model;

/**
 * Created by cyn  son 06/24/2016.
 */

public class PostalCode {
    private int zone_id;
    private String number;

    public int getZone_id() {
        return zone_id;
    }

    public void setZone_id(int zone_id) {
        this.zone_id = zone_id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "PostalCode{" +
                "zone_id=" + zone_id +
                ", number='" + number + '\'' +
                '}';
    }
}
