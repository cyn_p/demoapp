package mx.aliada.services.model;

import java.util.Date;

/**
 * Created by Cynthia Palma on 23/06/16.
 */

public class Service {
    private int id;
    private boolean confirmed;
    private double estimated_hours;
    private Date datetime;
    private Date ending_datetime;
    private String service_type;
    private Address address;
    private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public double getEstimatedHours() {
        return estimated_hours;
    }

    public void setEstimatedHours(double estimatedHours) {
        this.estimated_hours = estimatedHours;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public Date getEndingDate() {
        return ending_datetime;
    }

    public void setEndingDate(Date endingDate) {
        this.ending_datetime = endingDate;
    }

    public String getType() {
        return service_type;
    }

    public void setType(String type) {
        this.service_type = type;
    }

    public double getEstimated_hours() {
        return estimated_hours;
    }

    public void setEstimated_hours(double estimated_hours) {
        this.estimated_hours = estimated_hours;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Service{" +
                "id=" + id +
                ", confirmed=" + confirmed +
                ", estimated_hours=" + estimated_hours +
                ", datetime=" + datetime +
                ", ending_datetime=" + ending_datetime +
                ", service_type='" + service_type + '\'' +
                ", address=" + address.toString() +
                ", user=" + user.toString() +
                '}';
    }
}
