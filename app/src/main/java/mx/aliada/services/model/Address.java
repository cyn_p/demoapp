package mx.aliada.services.model;

/**
 * Created by cyn  son 06/24/2016.
 */

public class Address {
    private String street;
    private String number;
    private String interior_number;
    private String colony;
    private String city;
    private PostalCode postal_code;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getInterior_number() {
        return interior_number;
    }

    public void setInterior_number(String interior_number) {
        this.interior_number = interior_number;
    }

    public String getColony() {
        return colony;
    }

    public void setColony(String colony) {
        this.colony = colony;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public PostalCode getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(PostalCode postal_code) {
        this.postal_code = postal_code;
    }

    @Override
    public String toString() {
        return "Address{" +
                "street='" + street + '\'' +
                ", number='" + number + '\'' +
                ", interior_number='" + interior_number + '\'' +
                ", colony='" + colony + '\'' +
                ", city='" + city + '\'' +
                ", postal_code=" + postal_code.toString() +
                '}';
    }
}
