package mx.aliada.services.model;

import mx.aliada.general.GeneralProfile;

/**
 * Created by cyn  son 06/24/2016.
 */

public class User extends GeneralProfile {
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + getId() +
                ", email='" + getEmail() + '\'' +
                ", full_name='" + getFullName() + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
